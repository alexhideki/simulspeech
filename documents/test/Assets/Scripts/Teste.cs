﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Teste : MonoBehaviour {
	
	private bool isLoading;

	public float TimeSelectCircle = 0f;

	public Material Select, Unselect;

	public Image LoadCircle;

	public string SceneChageto;

	public AudioSource loading;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isLoading && TimeSelectCircle <= 1f) {
			TimeSelectCircle += Time.deltaTime;
			LoadCircle.fillAmount = TimeSelectCircle;
			if (TimeSelectCircle > 0.99f) {
				SceneManager.LoadScene (SceneChageto);
			}
				
		}
	}
		
	public void StartLoad(){
		isLoading = true;
		GetComponent<Renderer> ().material = Select;
		loading.Play ();
	}

	public void ResetLoad(){ 
		loading.Stop();
		GetComponent<Renderer> ().material = Unselect;
		isLoading = false; 
		TimeSelectCircle=0f;
		LoadCircle.fillAmount = TimeSelectCircle;
	}

}
